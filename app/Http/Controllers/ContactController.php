<?php

namespace App\Http\Controllers;

use App\UserContact;
use App\UserContactPhone;

use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Validator;


use App\Helpers\ImageHelper;

use Auth;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contact = $model = new UserContact();
        return view('layouts.form-wrapper', compact('contact', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     * Since the phone fields are an input array a different approach was needed. Refer to readme.md
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $validatedData = $request->validated();
        $errors = [];
        if ($contact = UserContact::create($validatedData)) {
            $this->uploadUserFiles($request, $contact);
            $request->merge(['user_contact_id'=>$contact->id]);
            $errors = $this->processPhoneFields($request);
            $request->session()->flash('success', trans('contact.created', ['name'=>$contact->name]));
        }
        return redirect(route('contact.edit', [$contact->uuid]))->withInput()->withErrors($errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(UserContact $contact)
    {
        $this->authorize('view', $contact);

        return view('contact.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserContact $contact)
    {
        $model = $contact;
        $this->authorize('view', $contact);

        return view('layouts.form-wrapper', compact('contact', 'model'));
    }

    /**
     * Update the specified resource in storage.
     * Since the phone fields are an input array a different approach was needed. Refer to readme.md
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactRequest $request, UserContact $contact)
    {
        $this->authorize('update', $contact);
        $validatedData = $request->validated();
        $errors = [];
        if ($contact->update($validatedData)) {
            $this->uploadUserFiles($request, $contact);
            $request->merge(['user_contact_id'=>$contact->id]);
            $errors = $this->processPhoneFields($request);
            $request->session()->flash('success', trans('contact.updated', ['name'=>$contact->name]));
        }
       
        return redirect()->back()->withInput()->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $contact = UserContact::findOrFail($id);
        $this->authorize('delete', $contact);
        $index = $request->get('index');
        $name = $contact->name;
        if (!$contact->isDefaultAvatar()) {
            \Storage::deleteDirectory($contact->getContactFolder());
        }
        $success = false;
        $messageTitle = trans('contact.delete_error_title');
        $message = trans('contact.delete_error', ['name'=>$name]);
        if ($contact->delete()) {
            $messageTitle = trans('contact.deleted_title');
            $message = trans('contact.deleted_message', ['name'=>$name]);
            $success = true;
        }
        $redirectTitle = trans('contact.redirect_title');
        $redirectMessage = trans('contact.redirect_message');
        $redirectUrl = route('contact.index');
        return response()->json(compact('messageTitle', 'message', 'index', 'redirectTitle', 'redirectMessage', 'redirectUrl'));
    }

    /**
     * Search functionality
     * My approach to the search was to use a query scope and with the results I only fetch the table id
     * That id is cross referenced with data-id tags in the divs and show/hidden accordingly
     *
     *  @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $favorite = $request->get('favorite');
        $query = Auth::user()->contactList();
        if ($favorite == 1) {
            $query = $query->favorite($favorite);
        }
        $results = $query->search($request->get('searchValue'))
        ->groupBy('user_contacts.id') //since it is a large query this is a way to distinct the results
        ->pluck('user_contacts.id')->toArray();
        return response()->json(compact('results'));
    }

    /**
     * Function to process the multiple phone fields
     * If it was an SPA the validation would be better, otherwise this is the best I can do
     *
     * @param  \Illuminate\Http\Request  $request
     *
     */
    private function processPhoneFields($request)
    {
        if ($request->has('label') && count($request->get('label'))) {
            $preserveIds = [];
            $errorMessages = [];
            $errors = new \Illuminate\Support\MessageBag();

            foreach ($request->get('label') as $key=>$phoneField) {
                if (strpos($key, 'new-') !== false) {
                    $validator  =  $this->makeUserContactPhonesValidator($phoneField, $request->get('cell')[$key]);
                   
                    if (!$validator->fails()) {
                        $userContactPhone = UserContactPhone::create(
                            [
                            'user_contact_id' => $request->get('user_contact_id'),
                            'label' => $phoneField,
                            'cell' => $request->get('cell')[$key],
                            'user_id' => $request->get('user_id'),
                            ]
                        );
                        $preserveIds[] = $userContactPhone->id;
                    } else {
                        $errorMessages[] =$validator->messages()->get('*');
                    }
                } else {
                    $validator  =  $this->makeUserContactPhonesValidator($phoneField, $request->get('cell')[$key], $key);
                    // dd($validator);
                    if (!$validator->fails()) {
                        $userContactPhone = UserContactPhone::findOrFail($key);
                        $userContactPhone->label = $phoneField;
                        $userContactPhone->cell = $request->get('cell')[$key];
                        $userContactPhone->save();
                    } else {
                        $errorMessages[] =$validator->messages()->get('*');
                    }
                    $preserveIds[] = $key;
                }
            }
            if (count($errorMessages)) {
                foreach ($errorMessages as $messageInputArray) {
                    foreach ($messageInputArray as $messageArray) {
                        foreach ($messageArray as $key =>$message) {
                            $errors->add($key, $message);
                        }
                    }
                }
            }
            UserContactPhone::where('user_contact_id', $request->get('user_contact_id'))->whereNotIn('id', $preserveIds)->delete();
            return $errors;
        }
    }
    /**
     * Make validator for UserContactPhones
     */

    private function makeUserContactPhonesValidator($phoneField, $cellField, $checkId =null)
    {
        return Validator::make(
            ['label'=>$phoneField,'cell'=>$cellField],
            [
                        'label' => 'max:255',
                        'cell' => [
                            'required',
                            'max:255',
                            \Illuminate\Validation\Rule::unique('user_contact_phones')
                            ->ignore($checkId)->where(function ($query) {
                                return $query->where('user_id', Auth::user()->id);
                            })
                            ]
                                // 'cell' => 'unique:user_contact_phones,cell,'.$checkId.',user_id,'.$userId.'|max:255',
                        ],
            ['cell.unique'=>trans('validation.unique_additional')]
         );
    }

    /**
     * Unified function to handle the file upload
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Illuminate\Database\Eloquent\Model $model
     */
    private function uploadUserFiles(Request $request, $model)
    {
        $fileNames = ['avatar'];
        foreach ($request->all() as $name => $file) {
            if (in_array($name, $fileNames)) {
                if (!is_null($model->uuid)) {
                    $path = ($model->getContactFolder(true) );
                    if (\Storage::disk('local')->exists($path)  ) {
                        //delete all previous avatars
                        //this would be wrong if there more images stored in the folder, but for this user case it should do
                        \Storage::deleteDirectory($path);
                    }
                }
                $image = new ImageHelper($model, $request->file($name));
                $response = $image->getResponse();
                $model->$name = $response[0];
                $model->save();
            }
        }
    }
}
