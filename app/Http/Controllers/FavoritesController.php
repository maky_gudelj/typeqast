<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FavoritesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.favorites');
    }

    /**
     * Add or remove the contact as a favorite
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function favoriteTrigger(Request $request, $id)
    {
        $contact = \App\UserContact::findOrFail($id);
        $contact->update($request->all());
        $addClasses = $contact->favorite_class;
        $removeClasses = $contact->getReplaceFavoriteClass();
        $messageTitle = trans('contact.added_to_favorites_title');
        $message = trans('contact.added_to_favorites', ['name'=> $contact->name]);
        if($contact->favorite == false){
            $messageTitle = trans('contact.removed_from_favorites_title');
            $message = trans('contact.removed_from_favorites', ['name'=> $contact->name]);
        }
        return response()->json(compact('messageTitle', 'message', 'addClasses', 'removeClasses'));
    }
}
