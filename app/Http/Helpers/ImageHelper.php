<?php

namespace App\Helpers;

use Auth;
use File;
use Mail;
use Carbon\Carbon;
use Image;
use Storage;

class ImageHelper
{
    public function __construct($model, $files, $storeName=null)
    {
        $this->model = $model;
        $this->files = $files;
        $this->className = get_class($model);
        $this->path = null;
        $this->response = null;
        $this->storename = $storeName;
        $this->detectPath();
        $this->fileUpload();
    }

    /**
     * Convert blob to iamge
     * When using croppie.js it was necessary to place the image into the blob. That blob needs to be converted to image in order to be saved
     *
     * @param blob $blob
     *
     * @return string $filename
     */
    public static function blobToImage($blob)
    {
        $img = Image::make(($blob));
        $img->encode(self::getFormat($img->mime()));
        $folder = storage_path('app/public/temp-upload/');
        if (!\File::exists($folder)) {
            \File::makeDirectory($folder, $mod = 0777, true, true);
        }
        $filename = time().'.'.self::getFormat($img->mime());
        $filePath =$folder.$filename;
        $img->save($filePath, 60);
        return $filename;
    }

    /**
     * Process files for upload.
     *
     * @param DB Object(collection) $model
     * @param string                $path
     * @param array                 $files
     *
     * @return \Illuminate\Http\Response
     */
    public function fileUpload()
    {
        $model =  $this->model;
        $files =  $this->files;
        $folder = $this->path;
    
        $uploadedNames = array();
        
        if (!\File::exists($folder)) {
            \File::makeDirectory($folder, $mod = 0775, true, true);
        }
      

        if (is_array($files)) {
            $uploadedNames = array();
            $counter = 0;
            foreach ($files as $file) {
                if (is_array($file)) {
                    foreach ($file as $f) {
                        ++$counter;
                        if ($f !== null) {
                            $uploadedNames[] = $this->moveUploaded($f, $folder, $model, $counter);
                        }
                    }
                } else {
                    $uploadedNames[] = $this->moveUploaded($file, $folder, $model);
                }
            }
        } else {
            $uploadedNames[] = $this->moveUploaded($files, $folder, $model);
        }
        
        $this->response =  $uploadedNames;
    }

    /**
     * Move files from temp folder and rename them.
     *
     * @param file object           $file
     * @param string                $folder
     * @param DB object(collection) $model
     *
     * @return string $newName
     */
    private function moveUploaded($file, $folder, $model, $counter = 0)
    {
        if (!is_null($file)) {
            $diffMarker = time() + $counter;
            $extension = '';
            if (method_exists($file, 'getClientOriginalExtension')) {
                $extension= $file->getClientOriginalExtension();
                $filename = $file->getClientOriginalName();
            } else {
                $extension = self::getFormat(explode('.', $file)[1]);
                $filename = $file;
            }
            
            if (is_null($this->storename)) {
                $newName = str_slug($model->uuid).'-'.date('d-m-Y-H-i-s').'-'.$diffMarker.'.'.$extension;
            } else {
                $newName = str_slug($model->uuid) . '-' . $this->storename . '.' . $extension;
            }
            $path = "$folder/$newName";
            $this->resizeToAvatar($file->getRealPath(), $path);

            return $newName;
        }
    }


    private function resizeToAvatar($imgPath, $savePath)
    {
        $img = Image::make($imgPath);

        // tq note fit is crop and resized
        $img->fit(160);
        $img->save($savePath);
    }
    /**
     * Function to get the response from the outside
     */
    public function getResponse()
    {
        return $this->response;
    }


    private function detectPath()
    {
        $this->path =  storage_path('app/public/data/users/'.$this->model->user->uuid.'/contacts/' . $this->model->uuid);
    }

    /**
     * Get the image format
     *
     * @param string $mime
     * @return string
     */
    public static function getFormat($mime)
    {
        $allowed = ['gif' => 'image/gif', 'jpg' => 'image/jpeg', 'pjpg' => 'image/jpeg', 'png' => 'image/png'];
    
        if ($format = array_search($mime, $allowed, true)) {
            return $format;
        }
        return 'jpg';
    }
}
