<?php

namespace App\Helpers;

class ViewHelper
{
    

    /**
     * Helper functions that gets the old inputs (if they exist), and returns a key-value pairs so We can set
     * the input array in the view
     *
     * @param string $part
     *
     * @return array $filtered
     */
    public static function filterOldInputsWithPartOfKey($part='new-')
    {
        $filtered = [];
        $filteredLabel = [];
        $filteredCell = [];
        
        // dd(\Session::get('errors'));
        if (isset(\Session::all()['_old_input']) && count(\Session::get('errors')) > 0 && array_key_exists('label', \Session::all()['_old_input'])) {
            $filteredLabel = array_filter(\Session::all()['_old_input']['label'], function ($key) {
                return(strpos($key, 'new-') !== false);
            }, ARRAY_FILTER_USE_KEY);
            $filteredCell = array_filter(\Session::all()['_old_input']['cell'], function ($key) {
                return(strpos($key, 'new-') !== false);
            }, ARRAY_FILTER_USE_KEY);
        }

        foreach ($filteredLabel as $key =>$label) {
            $filtered[$key]= [
               'label' => $label,
               'cell' => $filteredCell[$key],
           ];
        }
        return $filtered;
    }
}
