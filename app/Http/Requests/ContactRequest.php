<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = $this->route('contact');
        $id = null;
        if(!is_null($model)){ $id =$model->id;}
        return [
            'name' => 'required|max:255',
            'email' => ['required','email:rfc','max:255',//dns -removed because of server issues with php-intl
            
                \Illuminate\Validation\Rule::unique('user_contacts')->ignore($id)->where(function ($query) {
                                return $query->where('user_id', \Auth::user()->id);
                            })
            ],
            'user_id' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
