<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Request;

class FormViewComposer
{
    
    /**
     * Create a new view composer.
     *
     * @return void
     */
    public function __construct()
    {
    

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        
        $formWrapperData = $this->detectMethod($view->model);
   
        $view->with('formWrapperData', $formWrapperData);
        
        $view->with('collections', array());
        $view->with('counter', 0);
      
    }
    
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    protected function detectMethod($model)
    {
       
        $formWrapperData = new \StdClass();
        $formWrapperData->controller = Request::segment(1);

        if(!$model->exists){
            $formWrapperData->action = 'store';
            $formWrapperData->method = 'post';
            
            $formWrapperData->formUrl = route(  Request::segment(1).'.store');

            $formWrapperData->title = trans('controller.'.$formWrapperData->controller);
            $formWrapperData->buttonMethod = trans('documentForm.saveContinue');

        }
        else{
            $formWrapperData->action = 'update';
            $formWrapperData->method = 'PATCH';
            $formWrapperData->form = '';
            $formWrapperData->formUrl = route(  Request::segment(1).'.update',[$model->uuid]);
            $formWrapperData->title = trans('controller.'.$formWrapperData->controller);
            $formWrapperData->buttonMethod = trans('documentForm.saveContinue');//trans('formWrapper.update');

        }
       
        return $formWrapperData;
    }
}
