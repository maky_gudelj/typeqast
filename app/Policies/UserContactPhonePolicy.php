<?php

namespace App\Policies;

use App\User;
use App\UserContactPhone;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserContactPhonePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any user contact phones.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the user contact phone.
     *
     * @param  \App\User  $user
     * @param  \App\UserContactPhone  $userContactPhone
     * @return mixed
     */
    public function view(User $user, UserContactPhone $userContactPhone)
    {
        return $user->id === $userContactPhone->userContact->user_id;
    }

    /**
     * Determine whether the user can create user contact phones.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the user contact phone.
     *
     * @param  \App\User  $user
     * @param  \App\UserContactPhone  $userContactPhone
     * @return mixed
     */
    public function update(User $user, UserContactPhone $userContactPhone)
    {
        return $user->id === $userContactPhone->userContact->user_id;
    }

    /**
     * Determine whether the user can delete the user contact phone.
     *
     * @param  \App\User  $user
     * @param  \App\UserContactPhone  $userContactPhone
     * @return mixed
     */
    public function delete(User $user, UserContactPhone $userContactPhone)
    {
        return $user->id === $userContactPhone->userContact->user_id;
    }

    /**
     * Determine whether the user can restore the user contact phone.
     *
     * @param  \App\User  $user
     * @param  \App\UserContactPhone  $userContactPhone
     * @return mixed
     */
    public function restore(User $user, UserContactPhone $userContactPhone)
    {
        return $user->id === $userContactPhone->userContact->user_id;
    }

    /**
     * Determine whether the user can permanently delete the user contact phone.
     *
     * @param  \App\User  $user
     * @param  \App\UserContactPhone  $userContactPhone
     * @return mixed
     */
    public function forceDelete(User $user, UserContactPhone $userContactPhone)
    {
        return $user->id === $userContactPhone->userContact->user_id;
    }
}
