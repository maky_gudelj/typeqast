<?php

namespace App\Policies;

use App\User;
use App\UserContact;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserContactPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any user contacts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the user contact.
     *
     * @param  \App\User  $user
     * @param  \App\UserContact  $userContact
     * @return mixed
     */
    public function view(User $user, UserContact $userContact)
    {
        return $user->id === $userContact->user_id;
    }

    /**
     * Determine whether the user can create user contacts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the user contact.
     *
     * @param  \App\User  $user
     * @param  \App\UserContact  $userContact
     * @return mixed
     */
    public function update(User $user, UserContact $userContact)
    {
        return $user->id === $userContact->user_id;
    }

    /**
     * Determine whether the user can delete the user contact.
     *
     * @param  \App\User  $user
     * @param  \App\UserContact  $userContact
     * @return mixed
     */
    public function delete(User $user, UserContact $userContact)
    {
        return $user->id === $userContact->user_id;
 
    }

    /**
     * Determine whether the user can restore the user contact.
     *
     * @param  \App\User  $user
     * @param  \App\UserContact  $userContact
     * @return mixed
     */
    public function restore(User $user, UserContact $userContact)
    {
        return $user->id === $userContact->user_id;
    }

    /**
     * Determine whether the user can permanently delete the user contact.
     *
     * @param  \App\User  $user
     * @param  \App\UserContact  $userContact
     * @return mixed
     */
    public function forceDelete(User $user, UserContact $userContact)
    {
        return $user->id === $userContact->user_id;
    }
}
