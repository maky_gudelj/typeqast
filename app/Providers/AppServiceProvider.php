<?php

namespace App\Providers;

use App\User;
use App\Observers\UserObserver;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        #register observer(s)
        // User::observe(UserObserver::class);

        #register view composer(s)
        view()->composer('layouts.form-wrapper', 'App\Http\ViewComposers\FormViewComposer');

    }
}
