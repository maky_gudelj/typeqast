<?php

namespace App\Providers;

use App\UserContact;
use App\Policies\UserContactPolicy;
use App\UserContactPhone;
use App\Policies\UserContactPhonePolicy;


use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        UserContact::class => UserContactPolicy::class,
        UserContactPhone::class => UserUserContactPhonePolicyContactPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
