<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    
    protected $fillable = ['user_id','name','email','avatar','favorite'];
    protected $appends = ['favorite_class'];


     /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) \Webpatser\Uuid\Uuid::generate(4);
        });
    }
     /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * Relations
     */
    /**
     * Get user
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * Get UserPhones
     */
    public function userPhones(){
        return $this->hasMany(UserContactPhone::class,'user_contact_id');
    }

     /**
     * Scope a query to order the contacts
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $direction
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderByName($query,$direction ='asc')
    {
        return $query->orderBy('name', $direction);
    }

     /**
     * Scope a to filter favorite contacts
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  bool  $favorite
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFavorite($query,$favorite =1)
    {
        return $query->where('favorite', $favorite);
    }

    /**
     * Scope for the ajax search
     */
    public function scopeSearch($query,$searchValue)
    {
        $searchValue = trim(strtolower($searchValue));
        return $query->select('user_contacts.id')
        ->join('user_contact_phones', 'user_contacts.id', '=', 'user_contact_phones.user_contact_id')
        ->whereRaw('LOWER(name) LIKE ?', ['%'.$searchValue.'%'] )//if whereRaw wasn't used it would give me an error that the field is not defiend,
        ->orWhereRaw('LOWER(label) LIKE ?', ['%'.$searchValue.'%'] )
        ->orWhereRaw('LOWER(cell) LIKE ?', ['%'.$searchValue.'%'] );
        
    }


    /**
     * Get the favorite classes that need to replace the existing ones (used for changing the icon)
     *
     * @return string
     */

    public function getfavoriteClassAttribute(){
        if($this->favorite == true){
            return 'fas remove ';
        }
        else{
            return 'far add ';
        }
    } 

    /**
     * Get the favorite classes that need to replace the existing ones (used for changing the icon)
     * 
     * @return string
     */
    public function getReplaceFavoriteClass(){
        if($this->favorite == true){
            return 'far add ';
        }
        else{
            return 'fas remove ';
        }
    } 

    /**
     * Avatar getter
     * 
    */
    public function getAvatarAttribute($value)
    {
    
        if (is_null($value) || $value == 'user.png') {
            if(is_null($value)){
                $value ='user.png';
            }
            return  asset('img/'.$value);
        }
        //return \Storage::url('data/users/'.$this->user->uuid.'/contacts/' . $this->uuid.'/'.$value);

        return  \Storage::url($this->getContactFolder().$value) ;
    }

    /**
     * Check if avatar is default
     * @return bool
    */
    public function isDefaultAvatar()
    {  

        if (is_null($this->avatar) || strpos($this->avatar,'user.png') === false ) {
            return false;
        }
        return true;
    }

    /**
     * Check if avatar is default
     * @return bool
    */
    public function getContactFolder($storage = false)
    {  
        $additionalPath = '';
        if($storage == true){
            $additionalPath = 'public/';

        }
        
        return $additionalPath.'data/users/'.$this->user->uuid.'/contacts/' . $this->uuid.'/';
    }
}
