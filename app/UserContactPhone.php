<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContactPhone extends Model
{
    protected $fillable = ['user_contact_id','user_id','label','cell'];

    //We can use also the $blaclisted ect. This method is used to simplify everything with bulk import

    /**
     * Relations
     */


    /**
     * Get userContact
     */
    public function userContact(){
        return $this->belongsTo(UserContact::class,'user_contact_id');
    }

}
