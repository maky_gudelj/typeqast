<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\UserContactPhone;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/**
 * 
 *    

 */
//'user_contact_id','label','cell'
$factory->define(UserContactPhone::class, function (Faker $faker) {
    $randomContact = \App\UserContact::all()->random();
    return [
        
        'user_contact_id' => $randomContact->id, //will return
        'user_id' => $randomContact->user_id,
        'label' => $faker->name,
        'cell' => $faker->e164PhoneNumber,
    ];
});
