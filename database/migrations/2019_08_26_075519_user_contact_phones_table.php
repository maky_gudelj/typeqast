<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserContactPhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_contact_phones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_contact_id');
            $table->unsignedBigInteger('user_id');
            $table->string('label');
            $table->string('cell');
            $table->timestamps();

            $table->foreign('user_contact_id')->references('id')->on('user_contacts')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_contact_phones', function (Blueprint $table) {
            $table->dropForeign(['user_contact_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('user_contact_phones');
    }
}
