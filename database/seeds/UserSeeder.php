<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

use App\User;
use App\UserContact;
use App\UserContactPhone;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        'name' => 'Marijan Gudelj',
        'email' => 'test@marijangudelj.com',
        'email_verified_at' => now(),
        'password' => bcrypt('test123') , // password
        'remember_token' => Str::random(10)]);

        factory(UserContact::class, 45)->create();
        factory(UserContactPhone::class, 150)->create();


        User::create([
        'name' => 'Typecast Team',
        'email' => 'tq@marijangudelj.com',
        'email_verified_at' => now(),
        'password' => bcrypt('test123') , // password
        'remember_token' => Str::random(10)]);

        factory(UserContact::class, 10)->create();
        factory(UserContactPhone::class, 50)->create();

    }
}
