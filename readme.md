# Typeqast developer test

## Built with  [Laravel v5.8.16 (2019-05-07)](https://github.com/laravel/laravel/compare/v5.8.3...master)

## Install instructions
- clone the directory from bitbucket
- setup the .env
- run composer install, npm install, npm run dev
- (don't forget to key generate)
- symlink the folder (php artisan storage:link)
- migrate and seed
### Login credential
- test@marijangudelj.com // test123
- tq@marijangudelj.com // test123 :this one is used just so you can test the Policies and maybe fetch the emails to test the unique for the contacts. The account has fewer contacts overall.

## Used composer and npm addons
### Composer
- webpatser/laravel-uuid : so We don't fetch the id directly-as a security measure)
- laravelcollectiove/html : for forms
- intravention/image 
### npm
- bootstrap 4
- sweetalert2
- toastr.js
- fontawesome

## Notes
### General
The app is built on top of Laravel framework with jQuery. (I found out later that it can be an SPA)
In order to display my knowledge a bit more I have created a form wrapper, with an ViewComposer to set the forms.
This way We can reuse existing forms making them more DRY.

### Contact store/update
Contact phone fileds are a input array.The the biggest problems were:
- processing new and updating the existing records (handled by forwarding the request into the a function and processing it there [DRY])
- Validating those fields
Also I would like to note, that due to the fact I approched this part I could not do the UpdateOrCreate method

My logic was that an UserContact can have an unique email and uniqe cell-but only for that user. So making it unique in the database was not an option and it hand to be done trough the validators.
To answer an question that will arrise: user_id in the user_contact_phones is stored there because when creating a validator I couldn't use a joing or with method on the \Rule -> thus making it almost impossible to filter the unique properly. This is my workaround

You will find a ViewHelper file where I have checked if there are old inputs from the input arrays to set them in the form-but they are only triggered if there are error messages, otherwise on create they would multiply.

### Search
Search is handled by a query scope and ajax.
On keyup there is a timeout so the form doesn't post immidielty. After that the approriate route is called, and filters the contacts either them being the "all contacts" or "my favorites". The most simplistic way I could think of is just to join the tables and to call the whereRaw() on the query. WhereRaw is used so I could get the values to lowercase from the database (so ti is case insensitive). If the db was PostgreSQL I could simply do it with 'ilike' param instead of 'like'. 

### a little security goes a long way
Every User and UserContact has an UUID. It is called instead of the $id. Also the files stored are symlinked to the storage.
So the filepath for user avatar would be (storage/app/public/) data/users/user-uuid/contacts/contact-uuid/avatar-image-name

### Image upload
For the image upload you may find that it was a bit over the top. The reason is that initially I used croppie.js. 
Croppie created blobs and I needed to parse those blobs into an temp image. Temp image would be processed after as a "normal" file. I decided to ditch that js part due to the fact that this is an backend developer test and went for a simple upload.
After the file is uploaded I used intravention/image to fit the 

## What I would have done differently
### UserModel (migration)
Separated the name into first and last name. For in order to display the first and last name I would either written and
accessor method and appended the full_name attribute to the model or create something like getFullName() to be called anytime so We don't mess around with the data. 
I have went only for the name column just because it was like that in the design.
### Model observers
Maybe you will see in the AppServiceProvider that there is an User model observer commented out. My initial plan was to get a model observer for UserContact (not User) and on deleting to check there if there is an image that is not the default one->delete it. But to keep it simple I have skipped that part out



