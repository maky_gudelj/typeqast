$(function () {
    require('bootstrap');
    window.Swal = require('sweetalert2'); 
    window.toastr = require('toastr'); 

    /*Bind laravel security token to ajax*/
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('#csrf-token').attr('content')
        }
    });
    /* End Bind laravel security token to ajax*/
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $('.favorite-trigger').on('click touchend',function(){
        var favButton = $(this), favContainerDiv = favButton.closest('.row-eq-height');
        url = $(this).data('url'), favorite = 1, favContainer = false;
        if (favButton.closest('.row-eq-height').hasClass('favorites')){
            favContainer = true;
        }
        if ($(this).hasClass('remove')){
            favorite = 0;
        }
        $.ajax({
            type: 'POST',
            url: url,
            data: { favContainer: favContainer, favorite: favorite},
            success: function (data,value) {
                favButton.removeClass(data.removeClasses);
                favButton.addClass(data.addClasses);
                toastr.success(data.message, data.messageTitle)
                console.log(favContainer);
                if (favContainer == true){
                    favButton.closest('.card-container').hide('slow').remove();
                    if (!favContainerDiv.find('.card-container').length){
                        $('.alert-info').removeClass('d-none');
                    }
                }
            },
            error: function (data) { },
        });
    });

    $('.delete-contact').on('click touchend',function(){
        var button = $(this),
            url = $(this).data('url'), title = $(this).data('title'), message = $(this).data('message'),index = $(this).data('index');
          
        Swal.fire({
            title: title,
            text: message,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {'index':index},
                    success: function (data) {
                        toastr.success(data.message, data.messageTitle);
                        if (data.index == 'false'){
                            toastr.info(data.redirectMessage, data.redirectTitle);
                            setTimeout(function(){
                                window.location.href = data.redirectUrl;
                            },2000);
                        }
                        else{
                            button.closest('.card-container').hide('slow').remove();
                        }
                    },
                    error: function (data) { },
                });
                
            }
        })
    });


    $('.add-number').on('click touchend',function(e){
        e.preventDefault();//form failsafe
        var clone = $('.clone').html(), append =$('.append');
        
        append.append(clone);
        append.find('.form-group.clone.d-none').removeClass('clone d-none');
        renameNumbers();

    });

    $('body').on('click touchend','.delete-number,.delete-circle',function(e){
        e.preventDefault();
        $(this).closest('.form-group').remove();
        renameNumbers();
    });
    function renameNumbers(){
        var numberDivs = $('.append .form-group.new');
        $.each(numberDivs, function(key,value){
            var number = key + 1;
            $.each($(this).find('input,select,textarea'), function (index, input) {
                input = $(input);
                $(this).removeAttr('disabled');
                $(this).attr('name', addNumberToInputs(input.attr('name'),
                    number));
            });
        });
    }

    function addNumberToInputs(name, number) {
        if (name.includes('[')) {
            name = name.split('[')[0];
        }
        return name + '[new-' + number+']';

    }

    /**
     * Search
     */
    $('.search-contacts').on('keyup',function(e){
        var searchValue = $(this).val(), url = $(this).data('url'), favorite = 0,
            searchInfoDiv = $('.search-info');
            /** added this so there is a small delay before We start the search. 
             * instead of multiple post request there will be one */
        searchInfoDiv.removeClass('d-none');
        setTimeout(function(){ 
            if ($('.row-eq-height').length && $('.row-eq-height').hasClass('favorites')) {
                favorite = 1;
            }
            if (searchValue == '') {
                $('.contact').removeClass('d-none');
                searchInfoDiv.addClass('d-none');
            }
            else {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: { searchValue: searchValue, favorite: favorite },
                    success: function (data) {
                        $('.contact').addClass('d-none');
                        $.each(data.results, function ($key, $val) {
                            $('.contact[data-id="' + $val + '"]').removeClass('d-none');
                        });
                        searchInfoDiv.addClass('d-none');
                    },
                    error: function (data) { },
                });
            }
        },500);
    });

    /**
     * Image upload
     */


    $('.img-upload').on('change', function () {
        
        readImageURL($(this));
        console.log('triggered');
            
    });
    // Start upload preview image
    function readImageURL(input) {
        var image = $('img.croppie-img'),imageDiv = $('.image-div');
        console.log(input);
        input = input[0];
        console.log(input);
        console.log(input.files);
        console.log(input.files[0]);
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                console.log(e.target.result);
                image.attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
            if (image.hasClass('d-none')){
                image.removeClass('d-none');
                imageDiv.addClass('d-none');
            }
            
            
            // console.log(reader.readAsDataURL(input.files[0]));
        }
    }
    var $uploadCrop,
        tempFilename,
        rawImg,
        imageId;
    function readFile(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();
            reader.onload = function (e) {
                $('.upload-image').addClass('ready');
                $('#cropImagePop').modal({ 'show': true });
                rawImg = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

   
    // End upload preview image


    // end Image
});