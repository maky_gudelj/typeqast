@extends('layouts.master')

@section('content')
<div class="row">

</div><!-- /.row -->
<hr class="colored">
<div class="row row-eq-height favorites">
    @if(count(Auth::user()->contactListfavorites) )
        @foreach(Auth::user()->contactListfavorites as $contact)
            @include('contact.partials.contact-card',compact('contact'))
        @endforeach
        
        <div class="col-12">
            <div class="alert-info text-center d-none">@lang('contact.no_favorites')</div><!-- /.info -->
        </div><!-- /.col-12 --> {{-- used as an ajax fallback in case that all favorites are removed--}}
    @else
       <div class="col-12">
            <div class="alert-info text-center">@lang('contact.no_favorites')</div><!-- /.info -->
        </div><!-- /.col-12 --> {{-- used as an ajax fallback in case that all favorites are removed--}}
    @endif
</div><!-- /.row -->
@stop