@extends('layouts.master')

@section('content')
<div class="row">
    
</div><!-- /.row -->
<div class="row row-eq-height">
    <div class="col-12 col-xs-6 col-md-3 card-container">
        <div class="card h-100 dashed-border">
            <div class="card-body h-100 vertical-align">
                <a href="{{route('contact.create')}}" class="text-center d-block font-size-1-5 no-underline">
                    <i class="fas fa-plus"></i><!-- /.fa fa-plus --> 
                    <br/>
                    @lang('contact.add_new')
                    
                </a>
            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div><!-- /.col-12 col-xs-6 col-md-3 -->
    
 
    @foreach(Auth::user()->contactList as $contact)
        @include('contact.partials.contact-card',compact('contact'))
    @endforeach
</div><!-- /.row -->
@stop