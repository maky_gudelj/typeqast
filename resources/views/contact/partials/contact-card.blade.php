<div class="col-12 col-xs-6 col-md-3 card-container contact" data-id="{{$contact->id}}">
    <div class="card h-100 ">
        <div class="card-body">
            <div class="card-title">
                <div class="row">
                    <div class="col">
                        <i class="{{$contact->favorite_class}} fa-heart favorite-trigger text-left card-icon no-color-change"
                            data-url="{{route('favorite-trigger',[$contact->id])}}"></i><!-- / fa-heart -->
                    </div><!-- /.col -->
                    <div class="col text-right">
                        <a href="{{route('contact.edit',$contact->uuid)}}" class="mr-0-5">
                            <i class="far fa-edit card-icon no-color-change"></i><!-- /.far fa-edit -->
                        </a>
                        <i class="fas fa-trash card-icon delete-contact no-color-change"
                            data-url="{{route('contact.delete',[$contact->id])}}"
                            data-title="@lang('contact.confirm_delete_title')"
                            data-message="@lang('contact.confirm_delete_message',['name'=>$contact->name])"
                            data-index="true"></i><!-- /.fa fa-edit -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.card-title -->
            <a href="{{route('contact.show',$contact->uuid)}}" class="no-underline colored">
                <div class="row">
                    <div class="col-6 offset-3 text-center">
                        <img src="{{$contact->avatar}}" alt="" class="img-fluid img-thumbnail rounded-circle">
                    </div><!-- /.col -->

                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <p class="text-center grey font-size-1-2 mt-1 contact-name">
                            {{$contact->name}}
                        </p><!-- /.text-center -->
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </a>
        </div><!-- /.card-body -->
    </div> <!-- /.card -->
</div><!-- /.col-12 col-xs-6 col-md-3 -->