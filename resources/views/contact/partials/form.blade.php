<div class="container">
    <input type="hidden" name="user_id" value="{{Auth::user()->id}}"> {{--used to validate the request--}}
    <div class="row">
        <div class="col-md-3 text-center">
            <figure>
                @if($contact->isDefaultAvatar() == true)
                <div class="image-div img-fluid img-thumbnail rounded-circle circle-div"></div>
                <!-- /.croppie-img img-fluid img-thumbnail rounded-circle -->
                @endif
                <img src="{{$contact->avatar}}" class="croppie-img img-fluid img-thumbnail rounded-circle 
                @if($contact->isDefaultAvatar() == true) d-none @endif" alt="{{$contact->name}} avatar">
    
                <figcaption class="image-upload-figcaption">
                    <i class="fas fa-upload"></i>
                    <input type="file" class="file img-upload" name="avatar" />
            </figure>
        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="row">
                <div class="col"><a href="{{route('contact.index')}}"><i class="fas fa-level-up-alt rotated-left pull-left"></i></a></div><!-- /.col -->
                @if($model->exists)
                    <div class="col text-right"> 
                        <i class="fas fa-trash card-icon delete-contact" data-url="{{route('contact.delete',[$contact->id])}}"
                        data-title="@lang('contact.confirm_delete_title')"
                        data-message="@lang('contact.confirm_delete_message',['name'=>$contact->name])" data-index="false"></i></div><!-- /.col -->    
                @endif
            </div><!-- /.row -->
            
           
            <hr class="colored">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="full-name" class="text-primary full-name icon-before bolded">@lang('contact.full-name')</label>
                        <input type="text" class="form-control bigger-input" id="full-name" name="name" value="{{old('name',$contact->name)}}" required
                            placeholder="@lang('contact.full-name-placeholder')">
                    </div>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
    
            <hr class="colored">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email" class="text-primary email icon-before bolded">@lang('contact.email')</label>
                    <input type="email" class="form-control bigger-input" id="email" name="email" value="{{old('email',$contact->email)}}" required
                            placeholder="@lang('contact.email-placeholder')">
                    </div>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
    
            <hr class="colored">
            
                    <div class="form-group">
                        <label class="text-primary number icon-before bolded">@lang('contact.numbers')</label>
                    </div>
                    <div class="clone d-none">
                        <div class="form-group new">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control bigger-input"  name="label" disabled required
                                        placeholder="@lang('contact.number-capitalized')">
                                </div><!-- /.col-md-6 -->
                                <div class="col-md-5">
                                    <input type="tel" class="form-control bigger-input"  disabled required
                                        pattern="^[0-9-+\s()]*$" placeholder="@lang('contact.cell-capitalized')" name="cell">
                                </div><!-- /.col-md-5 -->
                                <div class="col-md-1 vertical-align">
                                    <div class="delete-circle">
                                        <i class="fas fa-times delete-number"></i>
                                    </div><!-- /.delete-circle -->
                                </div><!-- /.col-md-1 -->
                            </div><!-- /.row -->
                        </div><!-- /.form-group new -->
                    </div><!-- /.clone d-none -->
                  
                    <div class="append">
                        @if(count($contact->userPhones))
                            @foreach($contact->userPhones as $phoneContact)
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control bigger-input"  name="label[{{$phoneContact->id}}]"
                                               value="{{old('label['.$phoneContact->id.']',$phoneContact->label)}}"
                                                required placeholder="@lang('contact.number-capitalized')"> 
                                        </div><!-- /.col-md-6 -->
                                        <div class="col-md-5">
                                            <input type="tel" class="form-control bigger-input" id="cell" name="cell[{{$phoneContact->id}}]"
                                               pattern="^[0-9-+\s()]*$" value="{{old('cell['.$phoneContact->id.']',$phoneContact->cell)}}" required placeholder="@lang('contact.cell-capitalized')">
                                        </div><!-- /.col-md-5 -->
                                        <div class="col-md-1 vertical-align">
                                            <div class="delete-circle">
                                                <i class="fas fa-times delete-number"></i>
                                            </div><!-- /.delete-circle -->
                                        </div><!-- /.col-md-1 -->
                                    </div><!-- /.row -->
                                </div><!-- /.form-group -->
                            @endforeach
                        @endif
                        @if(count(ViewHelper::filterOldInputsWithPartOfKey()))
                            @foreach(ViewHelper::filterOldInputsWithPartOfKey() as $key =>$inputs)
                            <div class="form-group new">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control bigger-input" name="label[{{$key}}]"
                                            value="{{old('label['.$key.']',$inputs['label'])}}" required
                                            placeholder="@lang('contact.number-capitalized')">
                                    </div><!-- /.col-md-6 -->
                                    <div class="col-md-5">
                                        <input type="tel" class="form-control bigger-input" id="cell" name="cell[{{$key}}]" pattern="^[0-9-+\s()]*$"
                                            value="{{old('cell['.$key.']',$inputs['cell'])}}" required
                                            placeholder="@lang('contact.cell-capitalized')">
                                    </div><!-- /.col-md-5 -->
                                    <div class="col-md-1 vertical-align">
                                        <div class="delete-circle">
                                            <i class="fas fa-times delete-number"></i>
                                        </div><!-- /.delete-circle -->
                                    </div><!-- /.col-md-1 -->
                                </div><!-- /.row -->
                            </div><!-- /.form-group -->
                            @endforeach
                        @elseif(!count($contact->userPhones) || count(ViewHelper::filterOldInputsWithPartOfKey()) )
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control bigger-input" name="label[new-0]" required
                                        placeholder="@lang('contact.number-capitalized')">
                                </div><!-- /.col-md-6 -->
                                <div class="col-md-5">
                                    <input type="tel" class="form-control bigger-input" id="cell" name="cell[new-0]" required name="phone"
                                        pattern="^[0-9-+\s()]*$" placeholder="@lang('contact.cell-capitalized')">
                                </div><!-- /.col-md-5 -->
                                <div class="col-md-1 vertical-align">
                                    <div class="delete-circle">
                                        <i class="fas fa-times delete-number"></i>
                                    </div><!-- /.delete-circle -->
                                </div><!-- /.col-md-1 -->
                            </div><!-- /.row -->
                        </div><!-- /.form-group -->
                        @endif  
                    </div><!-- /.append -->

                    <div class="row">
                        <div class="col-12">
                           <div class="add-number-wrapper">
                                <button class="btn btn-primary add-number">
                                    <i class="fas fa-plus"></i> @lang('contact.add-number')
                                </button><!-- /.btn -->
                            </div><!-- /.add-number-wrapper --> 
                        </div><!-- /.col-12 -->
                    </div><!-- /.row -->
                    

                    <div class="row form-actions">
                        <div class="col">
                            <button type="reset" class="btn btn-primary">@lang('contact.cancel')</button><!-- /.btn btn-primary -->
                        </div><!-- /.col -->
                        <div class="col text-right">
                            <button type="submit" class="btn btn-primary pull-right">@lang('contact.save')</button><!-- /.btn btn-primary pull-right -->
                        </div><!-- /.col -->    
                    </div><!-- /.row -->
        </div><!-- /.col-md-9 -->
    </div><!-- /.row -->
</div><!-- /.container -->
