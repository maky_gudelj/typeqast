@extends('layouts.master')

@section('content')
    <div class="container half-screen-height">
        <div class="row">
            <div class="col-md-3 col-12 text-center">
                <img src="{{$contact->avatar}}" class="croppie-img img-fluid img-thumbnail rounded-circle " alt="{{$contact->name}} avatar">

            </div><!-- /.col-12 col-md-3 text-center -->
            <div class="col-md-9 col-12">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <a href="{{route('contact.index')}}"><i
                                class="fas fa-level-up-alt rotated-left pull-left"></i></a>

                        <span class="name grey ml-2">
                            {{ $contact->name }}
                        </span><!-- /.name -->
                    </div><!-- /.col -->
                
                    <div class="col-md-6 col-12 text-right icons-align">
                        <i class="{{$contact->favorite_class}} fa-heart favorite-trigger text-left card-icon"
                            data-url="{{route('favorite-trigger',[$contact->id])}}"></i><!-- / fa-heart -->
                        <a href="{{route('contact.edit',$contact->uuid)}}" class="ml-2">
                            <i class="far fa-edit card-icon ml-2"></i><!-- /.far fa-edit -->
                        </a>
                    </div><!-- /.col -->
                
                </div><!-- /.row -->


                <hr class="colored">
                
                <div class="row mt-5 font-size-1-2">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email" class="text-primary email icon-before bolded">@lang('contact.email')</label>
                            <span class="grey ml-2">{{$contact->email}}</span><!-- /.name -->
                            
                        </div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.row -->


                <div class="form-group font-size-1-2">
                    <label class="text-primary number icon-before bolded">@lang('contact.numbers')</label>
                </div>
                
                    @if(count($contact->userPhones))
                        @foreach($contact->userPhones as $phoneContact)
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="grey ml-2 text-uppercase">
                                        {{$phoneContact->label}}
                                    </span><!-- /.grey upper-case -->
                                    <span class="grey ml-2 underline">
                                        <a href="tel:{{$phoneContact->cell}}">{{$phoneContact->cell}}</a>
                                    </span><!-- /.grey -->
                                   
                                </div><!-- /.col-md-6 -->
                                
                            </div><!-- /.row -->
                        </div><!-- /.form-group -->
                        @endforeach
                    @else
                        <p class="info">@lang('contact.no_numbers')</p><!-- /.info -->
                    @endif

            </div><!-- /.col-md-9 col-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->

@stop