@extends('layouts.master')

    @section('content')
        {!! Form::open([
        'url' => $formWrapperData->formUrl,
        'method' => $formWrapperData->method,
        'enctype' => 'multipart/form-data',
        'class' => 'horizontal-form']) !!}
           
            @if( view()->exists($formWrapperData->controller.'.partials.form') )
                @include($formWrapperData->controller.'.partials.form')
            @else
                <div class="alert alert-warning">
                    <p> There is no form defined</p>
                </div>
            @endif
        {!! Form::close() !!}

    @stop
    