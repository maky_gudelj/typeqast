<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container background-white bigger-container">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm tq-gradient">
            <div class="container text-center">
                <a class="navbar-brand" href="{{ url('/') }}">
                  <img src="{!! url('/img/Typeqast.svg') !!}" alt="" class="img-fluid">
                </a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                </div>
            </div>
        </nav>

        @if( Request::is('contact') || Request::is('favorites'))
        <div class="row">
            <div class="col-12">
                <ul class="nav justify-content-center sub-nav">
                    <li class="nav-item">
                        <a class="nav-link @if(Request::is('contact')) active @endif" href="{{route('contact.index')}}">@lang('contact.all_contacts')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::is('favorites')) active @endif" href="{{route('favorites.index')}}">@lang('contact.my_favorites')</a>
                    </li>
                </ul>
            </div><!-- /.col-12 -->
                
        </div><!-- /.row -->
        
        <div class="container bigger-container border-wrapper"><hr class="colored"></div><!-- /.container -->
        <div class="row search-form-row">
            <div class="col-md-4 offset-md-4">
                <div class="form-group">
                    <input type="text" 
                    class="form-control search-contacts bigger-input" 
                data-url="{{route('contact.search')}}"
                    placeholder="&#xf002">
                </div>
                <div class="form-group search-info d-none text-center text-primary fa-2x">
                    <p> <i class="fas fa-spinner fa-pulse"></i> @lang('contact.searching')</p>    
                </div><!-- /.form-group -->
            </div><!-- /.col-12 -->    
        </div><!-- /.row -->
        

        @endif

        <main class="py-4 @if( Request::is('contact') || Request::is('favorites')) modified-main-padding @endif">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
            
            
            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
            
            
            @if ($message = Session::get('info'))
            <div class="alert alert-info alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
            
            
            @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                Please check the form below for errors
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @yield('content')
        </main>    
      
        
    </div>
    </div><!-- /.container -->
    <script src="{{asset('js/app.js')}}"></script>

    
</body>

</html>