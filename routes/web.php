<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => ['web','auth']], function () {
    Route::post('contact/serach','ContactController@search')->name('contact.search'); //some problems arries with the destroy (it was not finding a model) so this is a small workaround
    Route::post('contact/delete/{id}','ContactController@destroy')->name('contact.delete'); //some problems arries with the destroy (it was not finding a model) so this is a small workaround
    Route::resource('contact','ContactController');
    
    Route::post('favorite-trigger/{id}','FavoritesController@favoriteTrigger')->name('favorite-trigger');
    Route::get('favorites','FavoritesController@index')->name('favorites.index');
});



